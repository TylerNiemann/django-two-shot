from django.shortcuts import render, redirect
from django.db.models import Count
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
@login_required
def list_receipt(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/listReceipt.html", context)


@login_required
def list_expense(request):
    expense_list = ExpenseCategory.objects.filter(owner=request.user).annotate(
        receipts_count=Count("receipts")
    )
    context = {
        "expense_list": expense_list,
    }
    return render(request, "receipts/listExpense.html", context)


@login_required
def list_account(request):
    account_list = Account.objects.filter(owner=request.user).annotate(
        receipts_count=Count("receipts")
    )
    context = {
        "account_list": account_list,
    }
    return render(request, "receipts/listAccount.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}

    return render(request, "receipts/createReceipt.html", context)


@login_required
def create_expense(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(commit=False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {"form": form}

    return render(request, "receipts/createExpense.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}

    return render(request, "receipts/createAccount.html", context)
